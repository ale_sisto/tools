AndroTotal Simple Command Line Client
=====================================

AndroTotal exposes a free-of-charge API through Mashape. Therefore,
you will need both an AndroTotal and Mashape API key. Read the
following for the details.

Requirements
------------

    $ pip install requests #using virtualenv is recommended


Procedure
---------

Set the `AT_API_KEY` and `MS_API_KEY` variables as follows:

  1. sign up via Facebook or Google Oauth at [andrototal.org/signin](http://andrototal.org/signin)

  2. [create a Mashape account](http://mashape.com)

  3. send an email to [info@andrototal.org](mailto:info@andrototal.org) containing

     * the email address you used to signin to andrototal.org (likely,
     your Facebook or Google email address)

     * your Mashape username

  4. get your [Mashape production key](https://www.mashape.com/keys)

  5. get your [AndroTotal key](http://andrototal.org/personal/api)


Queue an APK for scanning
-------------------------

To submit a sample for scanning, follow this procedure:

    $ python andrototal_cli.py scan -h
    usage: andrototal_cli.py scan [-h] -at-key AT_KEY -ms-key MS_KEY
                                  [-tag TAG [TAG ...]] [-force-scan] [-is-malware]
                                  files [files ...]

    positional arguments:
      files                 Suspicious Android APK files. Note that flags such as
                            tagging or is-malware will be applied to all the
                            files.

    optional arguments:
      -h, --help            show this help message and exit
      -at-key AT_KEY, -a AT_KEY
                            AndroTotal API key.
      -ms-key MS_KEY, -m MS_KEY
                            Mashape.com API production key.
      -tag TAG [TAG ...], -t TAG [TAG ...]
                            Tag a submitted samples. Space separated.
      -force-scan, -f       Whether a known APK should be scanned again
      -is-malware, -M       Whether the submitted APK is a malware


    $ python andrototal_cli.py -l DEBUG scan -at-key <...> -ms-key <...> path/to/sample.apk sample2.apk more/*samples.apk

    2013-07-05 17:25:04,126 [DEBUG] andrototal: Running command: scan
    2013-07-05 17:25:04,127 [DEBUG] andrototal: Mashape.com: JL8GKzZDRWd8TlBDPzniaUJP2kQJp4GC
    2013-07-05 17:25:04,128 [DEBUG] andrototal: AndroTotal.org: e7695f7015ff4459a72f9ed91e127b8f
    2013-07-05 17:25:04,128 [DEBUG] andrototal: Uploading file sample.apk
    2013-07-05 17:25:11,173 [DEBUG] andrototal: Scan response: {"resource": "10a6f3efc8bc40c1922facde7d055208"}
    2013-07-05 17:25:11,173 [DEBUG] andrototal: Uploading file sample2.apk
    2013-07-05 17:25:15,361 [DEBUG] andrototal: Scan response: {"resource": "e870c6748ca3409f84c9c9e1a91daf3f"}
    2013-07-05 17:25:15,361 [DEBUG] andrototal: Uploading file 40156a176bb4554853f767bb6647fd0ac1925eac.apk
    2013-07-05 17:25:19,355 [DEBUG] andrototal: Scan response: {"resource": "21d6c7234a184db6b8e52f2bab523787"}
    2013-07-05 17:25:21,519 [DEBUG] andrototal: Uploading file samples-3.apk
    2013-07-05 17:25:27,803 [DEBUG] andrototal: Scan response: {"resource": "ec5b3c94ed624d6993b52a50d63153fa"}


Get the status of a scan
------------------------

To ask for the status of an ongoing scan, follow this procedure:

    $ python andrototal_cli.py result -h
    usage: andrototal_cli.py result [-h] -at-key AT_KEY -ms-key MS_KEY
                                    hashes [hashes ...]

    positional arguments:
      resource_ids          The resource ID related to a queued scan (as returned
                            by the scan command)

    optional arguments:
      -h, --help            show this help message and exit
      -at-key AT_KEY, -a AT_KEY
                            AndroTotal API key.
      -ms-key MS_KEY, -m MS_KEY
                            Mashape.com API production key.

    $ python andrototal_cli.py -l DEBUG result -at-key <...> -ms-key <...> 31c8a7e190ed4ad984c9f0b1d2eb52d3

    2013-07-08 09:54:25,124 [DEBUG] andrototal: Running command: result
    2013-07-08 09:54:25,125 [DEBUG] andrototal: Mashape.com: JL8GKzZDRWd8TlBDPzniaUJP2kQJp4GC
    2013-07-08 09:54:25,125 [DEBUG] andrototal: AndroTotal.org: e7695f7015ff4459a72f9ed91e127b8f
    2013-07-08 09:54:25,125 [DEBUG] andrototal: Checking scan 31c8a7e190ed4ad984c9f0b1d2eb52d3
    2013-07-08 09:54:27,117 [DEBUG] andrototal: Scan result: {"sample": {"sha256": "36f4a1029d93e20ef064b46ecbd514e7ff843d9cee50f6334cd5ce249f78c7bc", "first_seen": "2013-06-20 16:11:09", "sha1": "f2bed732c841a2fff018f9b9c971bbeb994565f0", "md5": "f9ef730367a2fbf32c3403ad2af6255c"}, "test_count": 7, "success_count": 7, "tests": [{"status": "SUCCESS", "ended_at": "2013-07-05 18:14:15", "task_id": "69d8852e-2575-4e2c-a0e9-5774b38b7838", "antivirus": {"engine_version": "7.00.3", "name": "Dr.Web Anti-virus Light (free) 7.00.3", "developer": "Doctor Web, Ltd"}, "result": "Android.ADRD.17 ", "started_at": "2013-07-05 18:11:53"}, {"status": "SUCCESS", "ended_at": "2013-07-05 18:14:38", "task_id": "0d6a00f3-0cf1-4185-b5e1-0af85a3fc897", "antivirus": {"engine_version": "9.36.28", "name": "Kaspersky Mobile Security Lite 9.36.28", "developer": "Kaspersky Lab"}, "result": "Trojan-Spy.AndroidOS.Adrd.u", "started_at": "2013-07-05 18:13:24"}, {"status": "SUCCESS", "ended_at": "2013-07-05 18:12:54", "task_id": "54d094f8-b7fc-4bbf-8a10-4d4eb2e2aa8f", "antivirus": {"engine_version": "2.0.3917", "name": "avast! Mobile Security 2.0.3917", "developer": "AVAST Software"}, "result": "Android:Adrd-G [Trj]", "started_at": "2013-07-05 18:11:50"}, {"status": "SUCCESS", "ended_at": "2013-07-05 18:16:12", "task_id": "25efe206-8b2f-409b-8bc1-811c7f62b094", "antivirus": {"engine_version": "3.3.4.970", "name": "Norton Security & Antivirus 3.3.4.970", "developer": "NortonMobile"}, "result": "THREAT_FOUND", "started_at": "2013-07-05 18:14:15"}, {"status": "SUCCESS", "ended_at": "2013-07-05 18:14:08", "task_id": "ebb850c8-ca4a-42a1-afd0-35a6d6620915", "antivirus": {"engine_version": "10.4.41", "name": "Kaspersky Mobile Security 10.4.41", "developer": "Kaspersky Lab"}, "result": "HEUR:Trojan-Spy.AndroidOS.Adrd.a", "started_at": "2013-07-05 18:11:55"}, {"status": "SUCCESS", "ended_at": "2013-07-05 18:14:11", "task_id": "d66d3b5c-041d-49f6-bd5a-d236e546176a", "antivirus": {"engine_version": "3.1", "name": "Mobile Security & Antivirus 3.1", "developer": "Trend Micro"}, "result": "AndroidOS_Adrd.HRY", "started_at": "2013-07-05 18:12:22"}, {"status": "SUCCESS", "ended_at": "2013-07-05 18:14:43", "task_id": "b1a23691-e8f9-469e-aadf-84ec310d83d1", "antivirus": {"engine_version": "1.8.0", "name": "Zoner AntiVirus Free 1.8.0", "developer": "ZONER, Inc."}, "result": "Trojan.AndroidOS.Adrd.A ", "started_at": "2013-07-05 18:12:54"}], "failure_count": 0}


Get the results of an existing analysis
---------------------------------------

To ask for the results of an existing analysis, follow this procedure:

    $ python andrototal_cli.py analysis -husage: andrototal_cli.py analysis [-h] -at-key AT_KEY -ms-key MS_KEY
                                      hashes [hashes ...]

    positional arguments:
      hashes                MD5, SHA-1 or SHA-256 of one or more APK samples

    optional arguments:
      -h, --help            show this help message and exit
      -at-key AT_KEY, -a AT_KEY
                            AndroTotal API key.
      -ms-key MS_KEY, -m MS_KEY
                            Mashape.com API production key.

    $ python andrototal_cli.py -l DEBUG analysis -at-key <...> -ms-key <...> cbdf63b2e5666799c4b74a8cd15565dd
    2013-07-08 09:59:18,416 [DEBUG] andrototal: Running command: analysis
    2013-07-08 09:59:18,417 [DEBUG] andrototal: Mashape.com: JL8GKzZDRWd8TlBDPzniaUJP2kQJp4GC
    2013-07-08 09:59:18,417 [DEBUG] andrototal: AndroTotal.org: e7695f7015ff4459a72f9ed91e127b8f
    2013-07-08 09:59:18,417 [DEBUG] andrototal: Retrieving analysis cbdf63b2e5666799c4b74a8cd15565dd
    2013-07-08 09:59:19,824 [DEBUG] andrototal: Analysis: {"sample": {"sha256": "d11de9bb4d7451ffe7e4b6bd6bab529e7411e3dbe90d468243ef87a5ed98941e", "first_seen": "2013-05-08 17:05:43", "sha1": "d9c2bc199769f8e1c817ccd23f1860f5125bdaf6", "md5": "cbdf63b2e5666799c4b74a8cd15565dd"}, "tests": [{"status": "SUCCESS", "ended_at": "2013-05-08 17:07:23", "task_id": "9bfd3cfe-8b54-4cbb-b1b4-4768fd408ce8", "antivirus": {"engine_version": "2.0.3917", "name": "avast! Mobile Security 2.0.3917", "developer": "AVAST Software"}, "result": "(Android:FakeInst-EO [PUP]). ", "started_at": "2013-05-08 17:05:53"}, {"status": "SUCCESS", "ended_at": "2013-05-08 17:07:23", "task_id": "473298e9-d31e-4c74-8cd9-3b818feab89b", "antivirus": {"engine_version": "7.00.3", "name": "Dr.Web Anti-virus Light (free) 7.00.3", "developer": "Doctor Web, Ltd"}, "result": "not a virus Adware.Startapp.origin.5 ", "started_at": "2013-05-08 17:05:48"}, {"status": "SUCCESS", "ended_at": "2013-05-08 17:07:19", "task_id": "b3b9c5bc-db4c-402e-9adb-a74457d41943", "antivirus": {"engine_version": "9.36.28", "name": "Kaspersky Mobile Security Lite 9.36.28", "developer": "Kaspersky Lab"}, "result": "NO_THREAT_FOUND", "started_at": "2013-05-08 17:05:49"}, {"status": "SUCCESS", "ended_at": "2013-05-08 17:06:43", "task_id": "f73bea40-5068-4bbd-b3fb-40c8b4bca608", "antivirus": {"engine_version": "3.3.4.970", "name": "Norton Security & Antivirus 3.3.4.970", "developer": "NortonMobile"}, "result": "NO_THREAT_FOUND", "started_at": "2013-05-08 17:05:49"}, {"status": "SUCCESS", "ended_at": "2013-05-08 17:07:23", "task_id": "131bd4fe-3bcd-4a72-a207-683ed8eb79f1", "antivirus": {"engine_version": "2.6.2", "name": "Mobile Security & Antivirus 2.6.2", "developer": "Trend Micro"}, "result": "AndroidOS_FakeInst.VTD", "started_at": "2013-05-08 17:05:52"}]}

Documentation
-------------

* [API documentation](https://www.mashape.com/andrototal/andrototal-apis)
