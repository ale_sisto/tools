#! /usr/bin/env python
'''
Documentation https://bitbucket.org/phretor/andrototal-tools
'''

# -- EDIT BELOW THIS LINE -- #
AT_API_KEY = None
MS_API_KEY = None
# -- DO NOT EDIT BELOW THIS LINE -- #


#! /usr/bin/env python

import sys
import os
import pprint
import logging
import logging.config

logger = logging.getLogger('andrototal')

try:
    import logutils
except ImportError:
    pass

try:
    import requests
except ImportError:
    sys.stderr.write('You must install "requests"\n')
    sys.exit(1)

try:
    import argparse
except ImportError:
    sys.stderr.write('You must either use Python 2.7 or install "argparse"\n')
    sys.exit(1)

API_ENDPOINT = 'https://andrototal-andrototal-apis.p.mashape.com'

def main():
    try:
        if sys.argv[2] == 'submit':
            submit_sample(
                sample_path=sys.argv[1],
                at_api_key=AT_API_KEY,
                ms_api_key=MS_API_KEY)
        elif sys.argv[2] == 'submit_tagged':
            submit_tagged_sample(
                sample_path=sys.argv[1],
                at_api_key=AT_API_KEY,
                ms_api_key=MS_API_KEY, \
                    tag=sys.argv[3])
        elif sys.argv[2] == 'status':
            get_status(
                resource=sys.argv[1],
                at_api_key=AT_API_KEY,
                ms_api_key=MS_API_KEY)
        else:
            raise IndexError()
    except IndexError:
        sys.stderr.write(
            'USAGE: python %s sample_path status|submit|[submit_tagged tag]\n' \
                % sys.argv[0])

class AndroTotalClient(object):
    def __call__(self):
        self._parse()
        self._configure_logger()
        self._run()

    def _parse(self):
        def _scan(args):
            self.command = 'scan'

        def _analysis(args):
            self.command = 'analysis'

        def _result(args):
            self.command = 'result'

        def HashType(value):
            if len(value) != 32:
                raise argparse.ArgumentTypeError('Invalid resource ID: len(%s) != 32' % value)
            return value

        def APIKeyType(key):
            if len(key) != 32:
                raise argparse.ArgumentTypeError('Invalid API key: len(%s) != 32' % key)
            return key

        def APKFileType(fname):
            path = os.path.realpath(fname)

            if not os.path.isfile(path):
                raise argparse.ArgumentTypeError('File %s does not exist' % fname)
            return path

        parser = argparse.ArgumentParser(
            description='AndroTotal Command Line Client')

        parser.add_argument(
            '-log-level',
            '-l',
            choices=['DEBUG',
                     'INFO',
                     'WARNING',
                     'ERROR'],
            help='Logging level.',
            default='INFO')

        subparsers = parser.add_subparsers(
            help='Available commands')

        scan = subparsers.add_parser(
            'scan',
            help='Queue a new basic scan')
        scan.set_defaults(func=_scan)

        analysis = subparsers.add_parser(
            'analysis',
            help='Get analysis by hash')
        analysis.set_defaults(func=_analysis)

        result = subparsers.add_parser(
            'result',
            help='Get the status and results of a queued scan')
        result.set_defaults(func=_result)

        for p in (scan, analysis, result):
            p.add_argument(
                '-at-key',
                '-a',
                type=APIKeyType,
                required=True,
                help='AndroTotal API key.')
            p.add_argument(
                '-ms-key',
                '-m',
                type=APIKeyType,
                required=True,
                help='Mashape.com API production key.')

        scan.add_argument(
            '-force-scan',
            '-f',
            default=False,
            action='store_true',
            help='Whether a known APK should be scanned again')

        scan.add_argument(
            '-is-malware',
            '-M',
            default=False,
            action='store_true',
            help='Whether the submitted APK is a malware')

        scan.add_argument(
            'files',
            type=APKFileType,
            nargs='+',
            help='Suspicious Android APK files. Note that flags such as '\
                'tagging or is-malware will be applied to all the files.')

        scan.add_argument(
            '-tag',
            '-t',
            nargs='+',
            help='Tag a submitted samples. Space separated.')

        result.add_argument(
            'resource_ids',
            type=HashType,
            nargs='+',
            help='The resource ID related to a queued scan (as returned by'\
                ' the scan command)')

        analysis.add_argument(
            'hashes',
            type=HashType,
            nargs='+',
            help='MD5, SHA-1 or SHA-256 of one or more APK samples')

        self.args = parser.parse_args()
        self.args.func(self.args)

        self.at_key = self.args.at_key
        self.ms_key = self.args.ms_key

    def _configure_logger(self):
        stream_handler = 'logging.StreamHandler'
        try:
            import logutils
            stream_handler = 'logutils.colorize.ColorizingStreamHandler'
        except ImportError:
            pass

        logging.config.dictConfig({
            'version': 1,
            'disable_existing_loggers': False,
            'formatters': {
                'standard': {
                    'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
                    },
                },
            'handlers': {
                'default': {
                    'level': getattr(logging, self.args.log_level, logging.DEBUG),
                    'class': stream_handler,
                    'formatter': 'standard'
                    },
                },
            'loggers': {
                'andrototal': {
                    'handlers': ['default'],
                    'level': getattr(logging, self.args.log_level, logging.DEBUG),
                    'propagate': False
                    },
                }
            })

    def _run(self):
        logger.debug('Running command: %s', self.command)
        logger.debug('Mashape.com: %s', self.ms_key)
        logger.debug('AndroTotal.org: %s', self.at_key)
        getattr(self, '_cmd_%s' % self.command)()

    def print_response(self, response):
        pprint.pprint(response.body)

    def _cmd_scan(self):
        for f in self.args.files:
            fn = os.path.basename(f)
            logger.debug('Uploading file %s', fn)

            variables = {
                'sample_apk': open(f),
                'is_malware': {False: 'false', True: 'true'}[self.args.is_malware],
                'force_scan': {False: 'false', True: 'true'}[self.args.force_scan],
                'tag': self.args.tag
                }

            r = requests.post(
                '%s/scan/queue-basic' % (API_ENDPOINT),
                headers={
                    'AndroTotal-Authorization': self.at_key,
                    'X-Mashape-Authorization': self.ms_key
                    },
                data=variables,
                files={'sample_apk': open(f)})

            logger.debug('Scan response: %s', r.text)

    def _cmd_analysis(self):
        for h in self.args.hashes:
            logger.debug('Retrieving analysis %s', h)

            variables = {
                'hash': h}

            r = requests.get(
                '%s/analysis' % (API_ENDPOINT),
                headers={
                    'AndroTotal-Authorization': self.at_key,
                    'X-Mashape-Authorization': self.ms_key
                    },
                params=variables)

            logger.debug('Analysis: %s', r.text)

    def _cmd_result(self):
        for h in self.args.resource_ids:
            logger.debug('Checking scan %s', h)

            variables = {
                'resource': h}

            r = requests.get(
                '%s/scan/result' % (API_ENDPOINT),
                headers={
                    'AndroTotal-Authorization': self.at_key,
                    'X-Mashape-Authorization': self.ms_key
                    },
                params=variables)

            logger.debug('Scan result: %s', r.text)


if __name__ == '__main__':
    sys.exit(AndroTotalClient()())
